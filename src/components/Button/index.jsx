import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

function Button({ children = null, className = '', onClick = () => {} }) {
  return (
    <button
      type="button"
      className={classNames(styles.btn, className)}
      onClick={onClick}
    >
      { children }
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
