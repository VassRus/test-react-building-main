import { expect } from '@jest/globals';

import { render, screen, fireEvent } from '@testing-library/react';
import Button from '.';

describe('Button snapshot testing', () => {
  test('should Button render', () => {
    const { asFragment } = render(<Button className="some-btn">Hello</Button>);

    expect(asFragment()).toMatchSnapshot();
  });
});

describe('Button onClick works', () => {
  test('should onClick works', () => {
    const callback = jest.fn();
    render(<Button onClick={callback}>Hello</Button>);

    const btn = screen.getByText('Hello');
    fireEvent.click(btn);
    expect(callback).toBeCalled();
  });
});
