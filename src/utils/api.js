import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://ajax.test-danit.com/api/v2/cards',
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_AUTH_TOKEN}`,
  },
});

export const addTodoApi = async (todo) => instance.post('', todo);

export const getAllTodoApi = async () => instance.get('');
